/**
 * Created by andreikuzmin on 28.3.17.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var rolesSchema = require('./comment').CommentSchema;

var Roles = new mongoose.Schema({
    admin: {
        type: Number,
        default: 0
    },
    user: {
        type: Number,
        default: 1
    }
});


exports.Roles = Roles;
