/**
 * Created by andreikuzmin on 17.3.17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var commentSchema = require('./comment').CommentSchema;



/*var Content = new Schema({
    text: {
        type : String

    }
});*/

var Content = new mongoose.Schema({
    text: String,
    userID: String,
    avgRating: [
        {
            type: Number,
            default: 0
        }
    ],
    comments: [commentSchema],
    nonVisible: {
        type: String

    }
});





exports.Content = mongoose.model('Content', Content);
