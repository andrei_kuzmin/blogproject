/**
 * Created by andreikuzmin on 20.3.17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var attachmentSchema = require('./attachments').AttachmentSchema;



var Comment = new Schema({
    author: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Account"
        }
    ],
    text: String,
    rating:
        {
            type: Number,
            default: 0
        }
    ,

    attachments: [attachmentSchema]

    });





//exports.Comment = mongoose.model('Comment', Comment);
exports.CommentSchema = Comment;
exports.CommentModel = mongoose.model('CommentModel', Comment);
