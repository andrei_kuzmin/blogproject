/**
 * Created by andreikuzmin on 15.3.17.
 */


var Role = require('./roles').Roles;
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
    nickname: {
        type : String,
        min : [3, 'Too short nickname'],
        max : [50, 'Too long nickname']

    },
    birthdate: {
        type : Date

    },
    role: {
        type: Number,
        default: 1
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);



























/*var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');
var bcrypt = require('bcrypt-nodejs');

var User = new Schema({
    username: {type: String, required: true},
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    created_at: Date,
    updated_at: Date
});


User.plugin(passportLocalMongoose);
module.exports = mongoose.model('User', User);*/


