/**
 * Created by andreikuzmin on 22.3.17.
 */
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var Attachment = new schema({
    data: {
        data: Buffer,
        contentType: String
    }
});

exports.AttachmentSchema = Attachment;