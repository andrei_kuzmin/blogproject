/**
 * Created by andreikuzmin on 23.3.17.
 */

var mongoose = require('mongoose');
var Content= require('./../models/content').Content;
var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../app');
var request = require('supertest');
var should = require('should');


chai.use(chaiHttp);


describe('Posts', function () {
    beforeEach(function (done) {
        Content.remove({}, function (err) {
            done();
        });
       done();

    });


    describe('/posted', function () {
        it('return status 200', function (done) {
            request(app)
                .get('/posted')
                .expect(200,done);

         });
    });
    
    describe('/posted', function () {
        it('Object expected as a result', function (done) {
            request(app)
                .get('/posted')
                .end(function (err, res) {
                    if(err){
                        throw err;
                    }
                    res.body.should.be.instanceOf(Object);
                    done();
                });
            
        });
    });



    describe('/leave_comment', function () {
        it('return status 500 when _id field is blank', function (done) {
            request(app)
                .post('/leave_comment')
                .expect(500, done)
                .end(function (err, res) {
                    if(err){
                        throw err;
                    }
                    done();
                });
        });
    });

    describe('/leave_comment', function () {
        it('return status 200 when _id field is filled', function (done) {
            request(app)
                .post('/leave_comment')
                .expect(200, done)
                .send({_id : "58d50427d4bf112b2ecd551b"}) //not tested
                .end(function (err, res) {
                    if(err){
                        throw err;
                    }
                    done();
                })
        })

    })


});



