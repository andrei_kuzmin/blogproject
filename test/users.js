/**
 * Created by andreikuzmin on 24.3.17.
 */
var mongoose = require('mongoose');
var Content = require('./../models/content').Content;
var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../app');
var request = require('supertest');
var should = require('should');


chai.use(chaiHttp);

describe('Users', function () {

    describe('/login_success', function () {
        it('return status 200', function (done) {
            request(app)
                .get('/login_success')
                .expect(200, done);
        });

    });

    describe('/login', function () {
        it('return status 200', function (done) {
            request(app)
                .get('/login')
                .expect(200, done);
        });
    });

    describe('/register', function () {
        it('error during register try with blank filds expects code: 500', function (done){
            request(app)
                .post('/register')
                .expect(500, done)
                .end(function (err, res) {
                    if(err){
                        throw err;
                    }
                    done();
                });
        });
    });

    describe('/login', function () {
        it('bad request when login with blank fields', function (done) {
            request(app)
                .post('/login')
                .expect(400, done)
                .end(function (err, res) {
                    if(err){
                        throw err;
                    }
                    done();
                })
        })
    })


});