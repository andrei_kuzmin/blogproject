var http = require("http");
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('cookie-session');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var app = express();
var mongooseConnection = require('./utils/mongooseConnection');
var Account = require('./models/account');
var errorHandler = require('./utils/errorUtils');
var flash = require('connect-flash');
var initPassport = require('./passport/init');





app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({keys: ['secretkey1', 'secretkey2', '...']}));
app.use(express.static(path.join(__dirname, 'public')));


//app.use(passport.initialize());
//app.use(passport.session());

app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

initPassport(passport);
//passport.use(new LocalStrategy(Account.authenticate()));



//passport.serializeUser(Account.serializeUser());
//passport.deserializeUser(Account.deserializeUser());


mongooseConnection.connect();


var userRoutes = require('./routes/users')(passport);
app.use('/', userRoutes);

app.use('/', require('./routes/routes'));
//app.use('/', require('./routes/users'));


app.use(function (err, req, res, next) {
    errorHandler.notFoundErrorHandler(err, req, res, next);
});
app.use(function (err, req, res, next) {
    errorHandler.internalServerErrorHandler(err, req, res, next);
});

module.exports = app;





























/*var express = require('express');
var path = require('path');
var http = require('http');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

var db = mongoose.connect('mongodb://localhost:27017/test');


var UserAccount = require('./models/account');
passport.use(new LocalStrategy(UserAccount.authenticate()));
passport.serializeUser(UserAccount.serializeUser());
passport.deserializeUser(UserAccount.deserializeUser());

mongoose.connection.once('connected', function() {
    console.log("Connected to database")
});


module.exports = app;*/


