/**
 * Created by andreikuzmin on 27.3.17.
 */

var fs = require('fs');
module.exports = {
    createComment : function (Content, req) {

        var commentText = req.body.commentInput;
        console.log(commentText);
        var commentRating = req.body.rating;
        console.log(commentRating);
        var attachment = {};
        if(req.file != null) {
            var filePath = req.file.path;
            attachment = fs.readFileSync(filePath);
            console.log(filePath);
        }
        Content.findByIdAndUpdate(req.body.postID, {new: true}, function (err, content) {
            if(err){
                console.log('error duriung findByID');
            }
            content.comments.push({text : commentText, rating: commentRating, attachments: { data : attachment, contentType : 'image/png'}});
            content.save(function (err) {
                if(err){
                    console.log('error while saving updated content model')
                }
            });
            console.log(content);

        });
    },

    deleteCommentById : function (Content, req, res) {


        if ( req.user.role == 0){

            Content.findByIdAndUpdate({_id: req.body.postID}, {new: true}, function (err, result) {
                if(err){
                    console.log(err);
                }

                debugger;
                var indexToDelete = result.comments.findIndex(function (element) {
                    return element._id == req.body.commentID;
                });


                result.comments.splice(indexToDelete, 1);

                result.save(function (err) {

                    if(err){
                        console.log('error during saving content model after deleting comment')
                    }
                });

                console.log('\n' + result + '\n');
            })
        }



        res.redirect('/posted');
    }
}
