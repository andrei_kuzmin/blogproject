/**
 * Created by andreikuzmin on 19.3.17.
 */
var mongoose = require('mongoose');
var configInitializer = require('./initConfig');
var express = require('express');
var app = express();
var config = configInitializer.init(app);
var dbconfig = config.configuration.dbconfig.fullURI;
//var dbconfig = config.configuration.dbconfig;


module.exports = {
    connect : function () {
        mongoose.connect(dbconfig, function (err) {
            if (err) {
                console.log('Could not connect to mongodb on localhost.' +
                    ' Ensure that you have mongodb running on localhost and mongodb accepts connections on standard ports!');
            }
        })

    }
};

//module.exports = mongoose;