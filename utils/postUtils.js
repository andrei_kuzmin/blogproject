/**
 * Created by andreikuzmin on 20.3.17.
 */
var fs = require('fs');
var checkVisibilityFlag = require('./../utils/utils');
var flash = require('connect-flash');


var getData = function  (Content,req, res, values, findRequest) {

    values = {text: 1, _id: 1, avgRating: 1, comments: 1, attachments: 1, userID: 1, nonVisible: 1};
    var pageContent = { postData : null, commentData : null, currentUserID : req.user._id, message: req.flash('message')};
    Content.find(findRequest, values, function (err, innerPostDocs) {
        if(err){
            console.log('error during querying');
        }






    });




};


var getPostLinks =  function (ids, req) {

    //var ids = getData(Content,req, res, values, findRequest);

    s = [];
    ids.forEach(function (id) {
        if (id.nonVisible == 'false' || req.user.role == 0){
            s.push('/viewPost/' + id._id)
        }
        else {
            if (id.userID == id.currentUserID){
                s.push('/viewPost'/ + id._id)
            }
        }


    });
    return s
};

module.exports =  {


    getData: function  (Content,req, res, values, findRequest) {

        values = {text: 1, _id: 1, avgRating: 1, comments: 1, attachments: 1, userID: 1, nonVisible: 1};
        var pageContent = { postData : null, commentData : null, currentUserID : req.user._id, message: req.flash('message')};
        Content.find(findRequest, values, function (err, innerPostDocs) {
            if(err){
                console.log('error during querying');
            }

            var links = getPostLinks(innerPostDocs, req);
            res.render('main', {postData: links});




        });




    },





    showPost : function (Content, req, res) {
        var pageContent = {postData : null};
        Content.findById(req.param("id"), {text: 1, comments: 1, attachments: 1}, function (err, innerPostDocs) {
            if(err){
                console.log('error during querying');
            }
            pageContent.postData = innerPostDocs;
            res.render('viewPost', pageContent);

        });

        
    },

    createPost : function (Content, req) {

        var postText = req.body.content;
        //console.log(postText);
        var flag = 'false';

        if (req.body.nonVisible === 'nonVisible'){

            flag = 'true';
            console.log(flag);

        }
        console.log(flag);
        var contentModel = new Content({
            text : postText,
            userID : req.user._id.toString(),
            nonVisible : flag
        });
        //console.log(typeof(req.user._id.toString()));

        //checkVisibilityFlag.checkAndSetPostVisibility(contentModel, req);
        //console.log(contentModel.nonVisible);
        contentModel.save(function (err) {
            if (err){
                console.log('error while saving post');
            }
            console.log('post saved');
        });
    },
    

    
    deleteAllPosts : function (Content, res) {
        Content.remove({}, function (err, result) {
           res.json({message : "removed"})
            
        });
        
    },
    
    deletePostByID : function (Content, req, res) {
        console.log(req.user.role);
        if (req.user.role == 0){
            Content.findOneAndRemove({_id: req.body.postID}, function (err, result) {
                console.log(req.body.postID);


            });
        }
        else{
            req.flash('message', 'You do not have enough permissions');
        }
        res.redirect('/posted');


    }




};

