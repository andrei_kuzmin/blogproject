/**
 * Created by andreikuzmin on 17.3.17.
 */

module.exports =  {

    init : function(app) {

    var config = require('../config/production.json');

    if (app.get('env') === 'development'){
        config = require('../config/development.json');
    }
    return config;

}

};
