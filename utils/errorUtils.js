/**
 * Created by andreikuzmin on 27.3.17.
 */

var app = require('../app');

module.exports = {
    notFoundErrorHandler : function (err, req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    },

    internalServerErrorHandler : function (err, req, res) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    }
};

