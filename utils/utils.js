/**
 * Created by andreikuzmin on 27.3.17.
 */

module.exports = {

    getRandomInt: function (min, max) {

        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    },



    splitTextList: function (list) {
        var formatedStrings = [];
        list.forEach(function (element) {

            formatedStrings.push(element);
        });
        return formatedStrings;
    },

    uploadImg: function (contentModel, _id, imgPath) {

    },

    checkAndSetPostVisibility: function (contentModel, req) {

        if (req.body.nonVisible === "Non-visible"){
            contentModel.nonVisible = 'true';

        }
        //console.log(contentModel.nonVisible);


    }
};
