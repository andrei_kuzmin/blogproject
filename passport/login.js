/**
 * Created by andreikuzmin on 29.3.17.
 */

var LocalStrategy = require('passport-local').Strategy;
var Account = require('./../models/account');
var bCrypt = require('bcrypt-nodejs');


module.exports = function (passport) {
    passport.use('login', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {

            Account.findOne({ 'username' :  username },
                function(err, user) {

                    if (err)
                        return done(err);
                    // Username does not exist, log error & redirect back
                    if (!user){
                        console.log('User Not Found with username '+username);
                        return done(null, false,
                            req.flash('message', 'User Not found.'));
                    }

                    if (!isValidPassword(user, password)){
                        console.log('Invalid Password');
                        return done(null, false,
                            req.flash('message', 'Invalid Password'));
                    }

                    return done(null, user);
                }
            );
        }));

    var isValidPassword = function(user, password){
        return bCrypt.compareSync(password, user.password);
    }
};

