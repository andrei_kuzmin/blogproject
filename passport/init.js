/**
 * Created by andreikuzmin on 29.3.17.
 */

var login = require('./login');
var signup = require('./signup');
var Account = require('./../models/account');

module.exports = function(passport){

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        console.log('serializing user: ');console.log(user);
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        Account.findById(id, function(err, user) {
            console.log('deserializing user:',user);
            done(err, user);
        });
    });


    login(passport);
    signup(passport);

};