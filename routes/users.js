var express = require('express');
var router = express.Router();
var accountUtils = require('./../utils/accountUtils');
var passport = require('passport');
var postUtils = require('./../utils/postUtils');
var Content = require('./../models/content').Content;
var Account = require('./../models/account');

/*router.get('/register', function(req, res) {
    res.render('register', {});
});

router.post('/register', function(req, res, next) {
    accountUtils.registerUser(Account, req);
    res.redirect('/posted');
});
router.post('/register', passport.authenticate('signup',{
    successRedirect: '/posted',
    failureFlash: true
}));



router.get('/login', function(req, res) {
    res.render('login', {user: req.user});
});

router.get('/login_success', function (req, res) {
    res.render('createPost', {});
});

router.post('/login', passport.authenticate('local'), function(req, res) {
    res.redirect('/posted');

});

router.post('/login', passport.authenticate('login',{
    successRedirect: '/posted',
    failureFlash: true
}));

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

router.get('/view', function (req, res) {
   postUtils.showAllData(Content, req, res);

});



module.exports = router;*/

module.exports = function (passport) {

    router.get('/register', function(req, res) {
        res.render('register', {});
    });


    router.post('/register', passport.authenticate('signup',{
        successRedirect: '/posted',
        failureRedirect: '/register',
        failureFlash: true
    }));



    router.get('/login', function(req, res) {
        res.render('login', {user: req.user});
    });

    router.get('/login_success', function (req, res) {
        res.render('createPost', {});
    });



    router.post('/login', passport.authenticate('login',{
        successRedirect: '/posted',
        failureRedirect: '/login',
        failureFlash: true
    }));

    router.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    router.get('/view', function (req, res) {
        postUtils.showAllData(Content, req, res);

    });

    return router;
    
};
