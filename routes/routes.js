var Content = require('./../models/content').Content;
var Comment = require('./../models/comment').CommentModel;
var express = require('express');
var postUtils = require('./../utils/postUtils');
var multer = require('multer');
var router = express.Router();
var commentUtils = require('./../utils/commentUtils');




router.get('/', function(req, res) {
    res.render('index', {user: req.user});
});

router.get('/posted', function (req,res) {
    var values = {_id: 1, nonVisible: 1, userID: 1, currentUserID: req.user};
    var findRequest = {};
    postUtils.getData(Content, req, res, values, findRequest);




});

router.get('/viewPost/:id', function (req, res) {
    postUtils.showPost(Content, req, res)
});

router.post('/article_post', function (req, res) {
    postUtils.createPost(Content, req);
    res.redirect('/posted')

});

router.post('/leave_comment', multer({ dest: './uploads/'}).single('filePath'), function (req, res) {
    commentUtils.createComment(Content, req);
    res.redirect('/posted')
});

router.post('/delete_post', function (req, res) {

   postUtils.deletePostByID(Content, req, res);
});

router.post('/delete_comment', function (req, res) {
   commentUtils.deleteCommentById(Content, req, res);
});
module.exports = router;


























